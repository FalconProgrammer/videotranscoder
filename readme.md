usage: transcode.py [-h] [--out-diff-dir OUT_DIFF_DIR]
                    [--extensions EXTENSIONS]
                    dir

Converts all videos in directory to X265

positional arguments:
  dir                   Input Directory

optional arguments:
  -h, --help            show this help message and exit
  --out-diff-dir OUT_DIFF_DIR
                        Output in [dir]_transcode directory
  --extensions EXTENSIONS
                        Comma separated list of file extensions to check for
