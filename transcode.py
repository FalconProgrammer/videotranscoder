import os, sys, subprocess, shlex, argparse, platform


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Converts all videos in directory to X265")
    parser.add_argument("dir", help="Input Directory")
    parser.add_argument("--out-diff-dir", help="Output in [dir]_transcode directory")
    parser.add_argument("--extensions", default="mp4,avi", help="Comma separated list of file extensions to check for")

    args = parser.parse_args()

    base_dir = args.dir
    
    # Determine extensions
    exts = [ ".{}".format(a) for a in args.extensions.split(",") ]

    # Remove trailing slash
    if base_dir[-1] == "/" or base_dir[-1] == "\\":
        base_dir = base_dir[:-1]

    # Check if input directory exists
    if not os.path.exists(base_dir):
        print("Directory does not exist: {}".format(base_dir))
        exit()

    # Create Transcode Directory
    if args.out_diff_dir:
        transcode_dir = base_dir + "_transcode"
    else:
        transcode_dir = base_dir

    if not os.path.exists(transcode_dir):
        os.makedirs(transcode_dir)

    # Walk all files/folders
    for root, dirs, files in os.walk(base_dir):
        for f in files:
            if "_X265" in f:
                print("Ignoring {} due to _X265 in filename".format(f))
                continue
                
            if os.path.splitext(f)[1] in exts:
            
                in_path = "{}/{}".format(root, f)
                out_dir = "{}/{}".format(transcode_dir, root[len(base_dir) + 1:])

                out_path = "{}/{}".format(out_dir, f[:-4] + "_X265.mp4")

                print("{} -> {}".format(in_path, out_path))

                if os.path.exists(out_path):
                    print("Already converted file. Skipping.")
                    continue

                if not os.path.exists(out_dir):
                    os.makedirs(out_dir)

                cmd = "ffmpeg -i \"{}\" -map 0 -c:v libx265 -c:s mov_text \"{}\"".format(in_path, out_path)
                print(cmd)

                if platform.system() == "Windows":
                    subprocess.call(cmd, shell=True)
                else:
                    subprocess.call(shlex.split(cmd))
